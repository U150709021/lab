package enes.shapes3d;

import enes.shapes.Circle;



public class Cylinder extends Circle {
	
	Cylinder cylinder;
	private int height;
	
	public Cylinder(int radius,int height){
		super(radius);
		this.height = height;
		
	}
	
public double area(){
	return 2 * super.area() + height * 2 ;
}

public double getRadius(){
	return radius;
}

public double volume(){
	return 2 * Math.PI * getRadius()* getRadius()* height;
}

public String toString(){
	
	return "r = " + radius + " h = " + height;
}

}
